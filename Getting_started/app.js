/* To build a web app with just vanilla javascript

const buttonEl = document.querySelector('button')
const inputEl = document.querySelector('input')
const listEl = document.querySelector('ul')

function addGoal(){
    const enteredValue = inputEl.value;
    const listItemEl = document.createElement('li');
    listItemEl.textContent = enteredValue;
    listEl.appendChild(listItemEl);
    inputEl = '';
}

buttonEl.addEventListener('click', addGoal)
 */

/* Re-build this app with Vue3 */
Vue.createApp({
  data() {
    return {
      goals: [],
      enteredValue: "",
    };
  },
  methods: {
    addGoal() {
      this.goals.push(this.enteredValue);
      this.enteredValue = "";
    },
  },
}).mount("#app");
